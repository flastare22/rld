import os
from dotenv import load_dotenv

from paho.mqtt.client import Client

gateways = ["E5", "92", "70"]
devices = ["arduino_1"]


def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    print("Hello 1!")
    client.subscribe(topic='+/devices/+/events/activations', qos=0)


def on_subscribe(client, userdata, mid, granted_qos):
    print("Subscribe")


def on_message(client, userdata, message):
    print("Received message '" + str(message.payload) + "' on topic '"
          + message.topic + "' with QoS " + str(message.qos))
    print((type(message.payload)))
    data = message.payload


def on_disconnect(client, userdata, rc):
    if rc != 0:
        print("Unexpected disconnection.")
    else:
        print("Disconnected with result code " + str(rc))


if __name__ == '__main__':

    load_dotenv()

    app_id = os.getenv("APP_ID")
    app_key = os.getenv("APP_KEY")

    if not os.path.exists("data"):
        os.makedirs("data")
        print("Directory data created")

    for device in devices:
        if not os.path.exists("data/{}".format(device)):
            os.makedirs("data/{}".format(device))
            with open("data/{}/index.txt".format(device)) as file:
                file.write('0')

    client = Client(client_id=app_id)
    client.username_pw_set(username=app_id, password=app_key)
    client.on_connect = on_connect
    client.on_subscribe = on_subscribe
    client.on_message = on_message
    client.on_disconnect = on_disconnect

    client.connect(host=os.getenv("HOST"), port=1883, keepalive=120)
    client.loop_forever()
    client.disconnect()

